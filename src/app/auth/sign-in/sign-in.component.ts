import { error } from '@angular/compiler/src/util';
import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { SharedService } from '@app/common/services/shared.service';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})

export class SigninComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    listMenuJson = [];

    constructor(private formBuilder: FormBuilder, private router: Router, private sharedService: SharedService) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    get fields() { return this.loginForm.controls; }

    onSubmit():void{
        const encryptedPassword=this.sharedService.encryptionAES(this.fields.password.value);

        this.sharedService.signInUserData(this.fields.username.value, encryptedPassword).subscribe(
            serverResponse=>{
                this.sharedService.setUserToken(serverResponse.body.token);
                this.sharedService.setHeader("Bearer "+serverResponse.body.token);
                var test = serverResponse.body.menuJson;

                for(const prop in test) {
                    this.listMenuJson.push(JSON.parse(test[prop]["menuJson"]) );    
                  }

                console.log(this.listMenuJson);
                
                this.sharedService.setMenuJson(this.listMenuJson);
                
                this.router.navigate(['app-home']);               
            },
            error => {
              globalThis.alert("Please give correct data");
              this.router.navigate(['sign-in']);
              
            }
        );
    }
    signUp():void{
        this.router.navigate(['sign-up']);
    }
}
