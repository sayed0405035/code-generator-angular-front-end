import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { SharedService } from '@app/common/services/shared.service';

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {

    registerForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private router: Router, private sharedService: SharedService) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            username: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get fields() { return this.registerForm.controls; }

    onSubmit():void{
        const encryptedPassword=this.sharedService.encryptionAES(this.fields.password.value);

        this.sharedService.signUpUserData(this.fields.username.value, this.fields.email.value , encryptedPassword).subscribe(
            serverResponse=>{
                this.router.navigate(['/sign-in']);
            },
            error => {
              globalThis.alert("Please give correct data");
              this.router.navigate(['sign-up']);
              
            }
        );
    }
}
