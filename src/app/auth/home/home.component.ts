import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-home', //added
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


    public screenWidth: any;
    public screenHeight: any;

    constructor() {
    }

    ngOnInit() {
        this.screenWidth = window.innerWidth;
        this.screenHeight = window.innerHeight;
        //console.log("in app-home");

    }

}