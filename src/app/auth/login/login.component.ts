import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {OAuth2AuthCodePKCE} from './../../common/custom-lib/oauth2-auth-code-PKCE';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    public isLoading: boolean = false;
    public oAuth2Obj: OAuth2AuthCodePKCE;


    constructor(private router: Router) {
    }

    ngOnInit() {
        //console.log("in login");
        this.router.navigate(['/sign-up']);
    }
}