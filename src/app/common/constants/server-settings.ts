import { environment } from './../../../environments/environment';

//export const authorizationServerUrl = environment.authUrl;
export const resourceServerUrl = environment.resourceUrl;
export const homeServerUrl = environment.homeUrl;
