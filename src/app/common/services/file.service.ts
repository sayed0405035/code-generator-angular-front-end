import {HttpBackend, HttpClient, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {getHttpHeadersForFile} from '@src/../e2e/src/common/constants';
import {environment} from '@src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class FileService {

    public withCredentials = false;

    constructor(private http: HttpClient, private handler: HttpBackend) {
        this.http = new HttpClient(handler);
    }

    upload(files: Array<File>, url: string, formData: FormData) {
        if (!files) {
            return;
        }
        if (environment.production) {
            this.withCredentials = true;
        }
        const req = new HttpRequest('POST', url, formData, {
            headers: getHttpHeadersForFile(),
            reportProgress: true,
            withCredentials: this.withCredentials
        });
        return this.http.request(req);
    }
}