import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders, HttpParams } from '@angular/common/http';
//import {Http, Headers} from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs';
import * as bcrypt from 'bcryptjs';
import {resourceServerUrl, homeServerUrl} from '@app/common/constants/server-settings'


@Injectable({
    providedIn: 'root'
})
export class SharedService {
    
    key: string = "z!!!!!!!1sdfadsf56adf456asdfasdf";
    appProperties = {
    VALUES: {
        KEY: "MTIzNDU2Nzg5MEFCQ0RFRkdISUpLTE1O",
        IV: "MTIzNDU2Nzg="
    }
    }
    //addition
    menuJsons =[];
    listItems = [];
    //end

    constructor(private http: HttpClient) { }

    public disableSource = new BehaviorSubject<any>(undefined);
    public notify: BehaviorSubject<string> = new BehaviorSubject<string>('');
    private userName = '';
    private currentLoggedInUserID = '';
    private currentUserCompanyOid = '';
    private rmsRole = '';
    private photoPath = '';
    private currentUserprofileName = '';
    private currentSectionName = '';

    private userToken = "";
    private headersInside: HttpHeaders;


    setNotify(value: string) {
        this.notify.next(value);
    }

    getNotify() {
        this.notify;
    }

    disableHeaderSidebar(disable: boolean) {
        this.disableSource.next(disable);
    }

    getcurrentLoggedInUserID(){
        return this.currentLoggedInUserID;
    }

    setcurrentLoggedInUserID(id: string){
        this.currentLoggedInUserID = id;
    }

    setCurrentUserCompanyOid(oid: string){
        this.currentUserCompanyOid = oid;
    }

    getCurrentUserCompanyOid(){
        return this.currentUserCompanyOid;
    }

    setCurrentUserprofileName(oid: string){
        this.currentUserprofileName = oid;
    }

    getCurrentUserprofileName(){
        return this.currentUserprofileName;
    }

    setCurrentSectionName(oid: string){
        this.currentSectionName = oid;
    }

    getCurrentSectionName(){
        return this.currentSectionName;
    }

    getRmsRole(){
        return this.rmsRole;
    }

    setRmsRole(rmsRole: string){
        this.rmsRole = rmsRole;
    }

    getName(){
        return this.userName;
    }

    setName(userName: string){
        this.userName = userName;
    }

    getPhotoPath(){
        return this.photoPath;
    }

    setPhotoPath(photoPath: string){
        this.photoPath = photoPath;
    }

    checkIfLockedAlreadyByOthers(item){
        return item.lockedBy != null && item.lockedBy !== this.getcurrentLoggedInUserID();
    }

    getLockIcon(item){
        if(item.lockedBy != null){
            return 'fa fa-lock fa-lg'
        }else{
            return 'fa fa-unlock fa-lg'
        }
    }

    getLockColor(item){
        if(this.checkIfLockedAlreadyByOthers(item)){
            return 'color: #9c9fa6'
        }else {
            return 'color: black'
        }
    }

    getApprovalRemarksIconColor(item){
        if(item.approverRemarks != null){
            return 'color: black'
        }
        else {
            return 'color: #9c9fa6'
        }
    }

    getLockIconTooltipText(item){
        if(!this.checkIfLockedAlreadyByOthers(item)){
            if(item.lockedBy != null){
                return 'Click to Unlock';
            } else{
                return 'Click to Lock';
            }
        }
    }

    getRemarksIconTooltipText(item){
        if(item.approverRemarks != null){
            return 'Click to See the  Approver Remarks'
        }
    }

    getCheckboxIconTooltipText(item){
        if(!this.checkIfLockedAlreadyByOthers(item)){
            return 'Click to Check'
        }
    }

    getDeleteIconToolTipText(){
        return 'Delete';
    }

    // new addition

    setUserToken(givenUserToken):void{
        this.userToken = givenUserToken;
    }
    getUserToken():string{
        return this.userToken;
    }
    encryptionAES (givenPassword) {
        const salt = "$2a$10$2eM9/38du44g0DJq.hgfg.";

        const pass = bcrypt.hashSync(givenPassword, salt);
        return pass;

    }

    encryptionAESWithSalt (givenPassword,salt) {
        const pass = bcrypt.hashSync(givenPassword, salt);
        return pass;
    }

    // getSalt(userName:string):void{

    // }

    signUpUserData(gotUserName:string, gotUserEmail:string, gotEncryptedPassword:string, gotaccessMenus: Array<string>):Observable<any>{
        const url=`${resourceServerUrl}/api/v1/auth/sign-up`;

        return this.http.post<any>(url, {

          "username": gotUserName,
          "email":gotUserEmail,
          "password": gotEncryptedPassword,
          "accessMenus": gotaccessMenus
        },
    
        {headers: this.getSignInUpHeader(), observe: 'response'});
      }
      signInUserData(name:string,encryptedPassword:string):Observable<any>{
        const url=`${resourceServerUrl}/api/v1/auth/sign-in`;
        return this.http.post(url,{"username": name,"password": encryptedPassword},
        {headers: this.getSignInUpHeader(), observe: 'response'});
      }

      setHeader(header:string):void{

        this.headersInside=new HttpHeaders().set('Authorization', header)
        .set('Access-Control-Allow-Origin', "*")
        .set('Access-Control-Allow-Methods', "DELETE, POST, GET, OPTIONS")
        .set('Access-Control-Allow-Headers', "Content-Type, Authorization, X-Requested-With");
     }
      getHeader(){
        return new HttpHeaders()
         .set('Content-Type', 'application/json')
         .set('Access-Control-Allow-Origin', '*')
         .set('Access-Control-Allow-Credentials', 'true')
         .set('Access-Control-Allow-Headers', 'Content-Type')
         .set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
         .set('Authorization', "Bearer "+this.getUserToken());
       }
      getSignInUpHeader(){
        return new HttpHeaders()
         .set('responseType', 'text')
         .set('Content-Type', 'application/json')
         .set('Access-Control-Allow-Origin', '*')
         .set('Access-Control-Allow-Credentials', 'true')
         .set('Access-Control-Allow-Headers', 'Content-Type')
         .set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        }

    setMenuJson(gotMenu : Array<any>):void{
        this.menuJsons = gotMenu;
        
    }
    getMenuJson() : Array<any>{
        return this.menuJsons;
        
    }

    checkPermissionMenu(menuName:string): boolean {
        for(const prop in this.menuJsons) {
            if(this.menuJsons[prop]["name"]==menuName["topMenuId"]){
                return true;
            }
            console.log(this.menuJsons[prop]["childs"].length);
            if(this.menuJsons[prop]["childs"].length>0){
                for(const child in  this.menuJsons[prop]["childs"]){
                    var retChild = this.checkPermissionMenu(this.menuJsons[prop]["childs"][child]["name"]);
                    if(retChild == true){return true;}
                }
            }
            
          }
        
        return false;
    }

    checkPermissionLink(menuLink:string): boolean {

        var currentMenuLink = homeServerUrl + menuLink;

        var baseLink = 'http://localhost:4200/app-home/dashboard';
        if( currentMenuLink == baseLink){return true;}

        for(const prop in this.menuJsons) {

            if(this.menuJsons[prop]["url"]==currentMenuLink){
                return true;
            }
            console.log(this.menuJsons[prop]["childs"].length);
            if(this.menuJsons[prop]["childs"].length>0){
                for(const child in  this.menuJsons[prop]["childs"]){
                    var retChild = this.checkPermissionLink(this.menuJsons[prop]["childs"][child]["url"]);
                    if(retChild == true){return true;}
                }
            }
            
          }
        return false;
    }

}
